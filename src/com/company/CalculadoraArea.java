package com.company;

public interface CalculadoraArea {
    double calcularArea();
}
