package com.company;

public class Triangulo extends FormaGeometrica implements CalculadoraArea{
    private double ladoA;
    private double ladoB;
    private double ladoC;

    public Triangulo(String nome) {
        super(nome);
    }

    public boolean validarTriangulo(){
        return (ladoA+ladoB) > ladoC
                && (ladoA + ladoC) > ladoB
                && (ladoB + ladoC) > ladoA;
    }

    @Override
    public double calcularArea() {
        double s = (ladoA + ladoB + ladoC) / 2;
        this.area = Math.sqrt(s * (s-ladoA) * (s-ladoB) * (s-ladoC));
        return this.area;
    }

    public void setLadoA(double ladoA) {
        this.ladoA = ladoA;
    }

    public void setLadoB(double ladoB) {
        this.ladoB = ladoB;
    }

    public void setLadoC(double ladoC) {
        this.ladoC = ladoC;
    }
}
