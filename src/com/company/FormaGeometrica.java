package com.company;

public class FormaGeometrica{
    protected String nome;
    protected double area;

    public FormaGeometrica(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public double getArea() {
        return area;
    }
}