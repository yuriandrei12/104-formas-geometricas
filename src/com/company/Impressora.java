package com.company;

import java.util.Scanner;

public class Impressora{
    Circulo circulo = new Circulo("Circulo");
    Retangulo retangulo = new Retangulo("Retangulo");
    Triangulo triangulo = new Triangulo("Triangulo");
    boolean contadorCirculo = true;
    boolean contadorRetangulo = true;
    boolean contadorTriangulo = true;
    private static String inicio;
    private static Scanner scan = new Scanner(System.in);


    public static void imprimir(FormaGeometrica formaGeometrica){
        System.out.println("A area do " + formaGeometrica.getNome() + " é " + formaGeometrica.getArea());
        System.out.println();
    }

    public void iniciar(){
        do{
            System.out.println("Digite a forma geométrica desejada(circulo, retangulo ou triangulo) ou sair para finalizar o programa");
            inicio = scan.next();
            String sair = "sair";
                if (!inicio.equals("sair")) {
                    escanear();
                }
        } while (!inicio.equals("sair"));
    }

    public void escanear(){
        if(inicio.equals("circulo")){
            if (contadorCirculo){
                System.out.println("Digite o diametro do circulo");
                double diametro = scan.nextDouble();
                circulo.setDiametro(diametro);
                circulo.calcularArea();
                contadorCirculo = false;
            }
            Impressora.imprimir(circulo);

        } else if(inicio.equals("retangulo")){
            if (contadorRetangulo){
                System.out.println("Digite a base do retangulo");
                double base = scan.nextDouble();
                retangulo.setBase(base);
                System.out.println("Digite a altura do retangulo");
                double altura = scan.nextDouble();
                retangulo.setAltura(altura);
                retangulo.calcularArea();
                contadorRetangulo = false;
            }
            Impressora.imprimir(retangulo);

        } else if(inicio.equals("triangulo")){
            if (contadorTriangulo){
                do {
                    System.out.println("Digite o tamanho do primeiro lado do triangulo");
                    double ladoA = scan.nextDouble();
                    triangulo.setLadoA(ladoA);
                    System.out.println("Digite o tamanho do segundo lado do triangulo");
                    double ladoB = scan.nextDouble();
                    triangulo.setLadoB(ladoB);
                    System.out.println("Digite o tamanho do terceiro lado do triangulo");
                    double ladoC = scan.nextDouble();
                    triangulo.setLadoC(ladoC);
                     if(!triangulo.validarTriangulo()){
                        System.out.println("Triangulo invalido, para um triangulo equilatero os lados devem ser iguais");
                         System.out.println();
                     } else {
                        triangulo.calcularArea();
                        contadorTriangulo = false;
                    }
                    } while (!triangulo.validarTriangulo());
            }
            Impressora.imprimir(triangulo);

        } else {
                System.out.println("Forma não encontrada.");
            }
    }
}
