package com.company;

public class Retangulo extends FormaGeometrica implements CalculadoraArea{
    private double base;
    private double altura;

    public Retangulo(String nome) {
        super(nome);

    }

    @Override
    public double calcularArea() {
        this.area = this.base * this.altura;
        return this.area;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }
}
