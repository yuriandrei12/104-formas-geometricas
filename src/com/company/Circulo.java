package com.company;

public class Circulo extends FormaGeometrica implements CalculadoraArea{
    private double diametro;

    public Circulo(String nome) {
        super(nome);
    }

    public double calcularArea(){
        double raio = this.diametro/2;
        this.area = Math.PI * (raio * raio);
        return this.area;
    }

    public void setDiametro(double diametro) {
        this.diametro = diametro;
    }
}
